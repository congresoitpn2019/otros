import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploBootstrapComponent } from './ejemplo-bootstrap.component';

describe('EjemploBootstrapComponent', () => {
  let component: EjemploBootstrapComponent;
  let fixture: ComponentFixture<EjemploBootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploBootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploBootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
