import { Component, OnInit } from '@angular/core';
import * as moment from "moment";

@Component({
  selector: 'app-ejemplo-moment-js',
  templateUrl: './ejemplo-moment-js.component.html',
  styleUrls: ['./ejemplo-moment-js.component.css']
})
export class EjemploMomentJsComponent implements OnInit {
	public hoy: any;

	constructor() { }

	ngOnInit() {
		this.hoy = moment(); 
	}

}
