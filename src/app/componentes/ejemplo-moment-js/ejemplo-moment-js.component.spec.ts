import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploMomentJsComponent } from './ejemplo-moment-js.component';

describe('EjemploMomentJsComponent', () => {
  let component: EjemploMomentJsComponent;
  let fixture: ComponentFixture<EjemploMomentJsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploMomentJsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploMomentJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
