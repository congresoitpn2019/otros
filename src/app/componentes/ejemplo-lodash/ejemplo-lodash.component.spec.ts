import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploLodashComponent } from './ejemplo-lodash.component';

describe('EjemploLodashComponent', () => {
  let component: EjemploLodashComponent;
  let fixture: ComponentFixture<EjemploLodashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemploLodashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploLodashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
