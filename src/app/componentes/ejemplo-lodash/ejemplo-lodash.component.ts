import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash'; 

@Component({
  selector: 'app-ejemplo-lodash',
  templateUrl: './ejemplo-lodash.component.html',
  styleUrls: ['./ejemplo-lodash.component.css']
})
export class EjemploLodashComponent implements OnInit {
	public alumnos: Array<{ nombre: string, carrera: string }> = [
		{ nombre: 'Miguel García', carrera: 'Mecatronica' }, 
		{ nombre: 'Juan Lopez', carrera: 'Ingenieria Industrial' }, 
		{ nombre: 'María Esquivel', carrera: 'Sistemas' }, 
		{ nombre: 'Carlos Dominguez', carrera: 'Sistemas' }
	]


  constructor() { }

  ngOnInit() {
  	var primero = _.first(this.alumnos);
  	console.log(primero);

  	var ultimo = _.last(this.alumnos); 
  	console.log(ultimo); 

  	var unicos = _.uniq(this.alumnos.map(_ => _.carrera));
  	console.log(unicos); 

  	var dosEnDos = _.chunk(this.alumnos, 2);
  	console.log(dosEnDos);

  	var sub = _.slice(this.alumnos, 1, 3); 
  	console.log(sub); 
  }

}
