import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms'; 


import { RouterModule, Routes } from '@angular/router'; 

import { AppComponent } from './app.component';
import { EjemploLodashComponent } from './componentes/ejemplo-lodash/ejemplo-lodash.component';
import { EjemploBootstrapComponent } from './componentes/ejemplo-bootstrap/ejemplo-bootstrap.component';
import { EjemploAngularBootstrapComponent } from './componentes/ejemplo-angular-bootstrap/ejemplo-angular-bootstrap.component';
import { EjemploMomentJsComponent } from './componentes/ejemplo-moment-js/ejemplo-moment-js.component';


const appRoutes: Routes = [
  {
    path: '', 
    redirectTo: '/lodash', 
    pathMatch: 'full'
  }, 
  {
    path: 'lodash', 
    component: EjemploLodashComponent
  }, 
  {
    path: 'bootstrap', 
    component: EjemploBootstrapComponent
  }, 
  {
    path: 'angular-bootstrap', 
    component: EjemploAngularBootstrapComponent
  }, 
  {
    path: 'moment-js', 
    component: EjemploMomentJsComponent 
  }
]


@NgModule({
  declarations: [
    AppComponent,
    EjemploLodashComponent,
    EjemploBootstrapComponent,
    EjemploAngularBootstrapComponent,
    EjemploMomentJsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
